SELECT 
    NEWID() AS id,
    getUTCdate() AT TIME ZONE 'UTC' AT TIME ZONE 'Aus Eastern Standard Time' AS createdate,
    CASE
        WHEN A.sms_permission = 'true' THEN 'C0700926'
        ELSE 'C0700928' 
    END AS mcid,
    A.original_cellular_no,
    A.service_number,
    A.firstname AS customer_name,
    Convert(varchar(30),getUTCdate()AT TIME ZONE 'UTC' AT TIME ZONE 'Aus Eastern Standard Time',112) + '020000' AS sms_date,
    A.saville_account_number,
    CASE
        WHEN A.sms_permission = 'true' THEN
            ('Hi '+ A.firstname + ' your Virgin Mobile Broadband service will close on 3 April 2020, after which time you will not be able to use the service. Find out how to switch to Optus [URL]. If you have a question, call us 136369. Unsub at virg.in/unsub.')
        ELSE 
            ('Hi '+ A.firstname + ' your Virgin Mobile Broadband service will close on 3 April 2020, after which time you will not be able to use the service. If you have a question, call us 136369. Unsub at virg.in/unsub.')
    END AS sms_text
FROM
    [PrepaidExitComms_EntryDE] A